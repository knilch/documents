- [ ] each t.requirement MUST be clear/unambiguous
- [ ] each t.requirement MUST be verifiable/confirmable
- [ ] each t.requirement MUST be unitary/cohesive
- [ ] each t.requirement MUST be traceable from need to implementation via t.evidence
- [ ] each t.requirement MUST be current/valid/true
- [ ] each t.requirement MAY be graded by priority/importance

