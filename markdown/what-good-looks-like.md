# What good looks like

Everything is:

- fit for purpose
  - legal and regulatory requirements are satisfied
  - technical requirements are satisfied
  - security and privacy requirements are safisfied
  - quality is guaranteed, exceptions are caught and addressed
- documented in writing, declaratively
  - as sentences/statements
  - or as configuration
  - or as scripts
  - at a single source of truth, with discoverable namespacing and glossary
  - visible to all contributors
  - traceable end-to-end, from business/stakeholder decisions to outputs
- version-controlled
  - with evidence of provenance
  - with effective branch-and-merge
  - with contributor identification and accountability
  - with evidence of independent pre-acceptance reviews, including all action history
- optimised
  - throughput is optimised
  - cycle times are mimimised
  - downtime is minimised
  - setup time is mimised
  - debugging time is minimised
  - learning time is mimised
  - rework and regressions are minimised
- originated as an issue/change
- specified and designed for availability and scale from the start

Issues/changes state:

- what the problem/requirement is
- what the expected solution is
- how the solution is implemented
- how the solution is verified/tested
- how the solution is landed/integrated

Construction is:

- declarative
- deterministic
- reproducible (at least functionally, ideally bit-for-bit)
- re-used where appropriate
- traceable with provenance and custody
- via shared ci pipeline infrastructure
- including all dependencies

Deployment + operation is:

- reproducible
- scripted/automated from the construction pipeline, including
  - atomic update
  - atomic rollback
  - atomic recovery

Compliance with all of the above is:

- measured via instrumentation
- with history
- with proof of integrity
