# WIP trustable:provenance approach

FIXME: aim towards worked example based on minimal distro

- show working CI/CD pipeline
- capture evidence of provenance for changing content
- capture evidence of reviews/approvals

FIXME:
- what about provenance for established/legacy content including FOSS
- what evidence do we need for provenance boundaries, e.g. supplier binaries?
