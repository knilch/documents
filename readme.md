# README

This repo contains WIP content describing a workflow for trustable software.
Working from the
[Trustable Hypothesis](https://gitlab.com/trustable/overview/wikis/hypothesis-for-software-to-be-trustable)
we care about several factors:

```mermaid
graph TD;
subgraph Trustability
factor1(Functionality)
factor2(Provenance)
factor3(Reliability)
factor4(Reproducibility)
factor5(Safety)
factor6(Security)
factor7(Compliance)
end

style factor1 fill:#fff,stroke:#333,stroke-width:1px
style factor2 fill:#fff,stroke:#333,stroke-width:1px
style factor3 fill:#fff,stroke:#333,stroke-width:1px
style factor4 fill:#fff,stroke:#333,stroke-width:1px
style factor5 fill:#fff,stroke:#333,stroke-width:1px
style factor6 fill:#fff,stroke:#333,stroke-width:1px
style factor7 fill:#fff,stroke:#333,stroke-width:1px

```

Currently we are working towards collection of evidence to help assess
the trustability of software including;

- requirements
  - how to create requirements
  - how requirements get reviewed
  - how requirements get accepted

- construction
  - requirements for software construction
  - how to construct software
  - how changes get reviewed
  - how changes get accepted and merged

- instantiation
  - requirements for software instantiation
  - how to instantiate software
  - how releases get reviewed
  - how releases get accepted and rolled out

## Licence

All documentation for the [trustable software project](https://trustable.io) is licenced
as CCO by default.
![CC Zero](http://i.creativecommons.org/p/zero/1.0/88x31.png)
